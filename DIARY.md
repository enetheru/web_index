Wed 13 Feb 2019 18:12:07 ACDT
-----------------------------
Now that i have some things that are mostly working i want to figure out what
the general workflow for the thing should be. this is the act of posting
something new, no maintenance or fixing things.

* drop a file in a content directory
* run generate
* fix up manual metadata like name, description, type
* run collate
* commit
* push

now most of that should be removed in favour of perhaps a single right click
menu item in caja. or a command line tool called publish

so i can call publish on any file and it will do all of the above.
or i can right click on any file and select publish.


Wed 20 Feb 2019 11:45:04 ACDT
-----------------------------
So after a bunch of thinking about how i want to make the visual appeal work
i'm thinking that rather than have the sorting done by clicking in the header,
i might make it a field like the filter list, something you can edit.. sorting
can be independent of view then, each of these rows for modifying the contents
will have to be able to hide, or be shown so something nice would be good for
that.

also my ideas on a grid based layout engine rather than generic things i really
would like to make but its a stretch.. i think i should focus on consolidating
what i ahve right now and then work on making it better after i have reached a
sort of okish milestone. maybe even keep it local only for the time being.

I would also like to have the interface differentiate between whether its
hosted on the web or hosted locally, for things like youtube videos that i
would like to publish locally but link them when its on the cloud, they would
have both a path and a url and depending on the source is what the url would
be.

hmm loads of brain storming over and over and over. my brain doesnt stop

Tue 12 Mar 2019 00:21:23 ACDT
-----------------------------
So i just had a thought of another field that can exist in the metadata, and
thats links to any other websites that something has been shared with, like
twitter, or mastodon. so when i hit publish, it can optionally send it to other
larger places.


Fri 22 Mar 2019 17:17:13 ACDT
-----------------------------
Unfortunately based on my experience so far, it seems improbable that it will
be useful to have such a generic interface, and a much easier to program one
will actually be more useful. thats not to say that the information that i have
gathered in this experiment wont be immensely useful. its just that trying to
shoehorn in multiple views into a single universal view with settings for all
attributes now seems like an overly generic solution that suffers from the
lowest common denominator.

I'm now having to re-think it entirely.. after struggling for weeks to
implement it.. oh well such is the way of things..sunk costs fallacy and all.


Thu 11 Apr 2019 11:18:37 ACST
-----------------------------
managing to pick this back up. I'm frustrated by the fact that computers are
inherently linear, and the table is multi dimentional.. so I think that to
achieve the results that i want I will have to make a sort of separation
between the display of the data, and the controls such that the controls are
overlayed ontop of the visual elements.

that way i can just have a simple display code, but the control code is much
heavier.

that way i can have column selection as a separate layer to say row selection,
duplicate them up from each other, and control them separately.

I think that will achieve my goals a lot cleaner than if i were to build the
display of the data and the controls together. it means yet another re-write
but also simpler display code. zero control code in the display.

Sun 14 Apr 2019 13:35:50 ACST
-----------------------------
ok so the display layer is up and running, and i really want to publish the
current state to the web, but the data is junk. I have to rebuild the database.

deploy script is shit just uses the current directory, i need to make it load
the configuration and then  use the source directory..

Fri 26 Apr 2019 10:03:33 ACST
-----------------------------
I think the reason I am having so much difficulty is because i have no
specified the problem accurately, that there are conflicting goals between
arbitrary and automatic column widths etc.

I think i might have to once again rejig my expectations, sigh.. tables are
kinda hard hey.

lets see if i can itemize the features i want to have
* resizable columns
* re-orderable columns
* sortable
* automatic proportional fit into arbitrary width
* overflow with scrollbars
* adding and removing columns
* rebuild when navigating to a folder, and back out.

since i am struggling with the proportional thing so much i am going to throw
it away, it only applies for the initial building/rebuilding of the table, and
once that is complete then isnt used again. so if i just stick to arbitrary
widths it is a lot easier to build.


Fri 26 Apr 2019 14:20:52 ACST
-----------------------------
I've come across another sort of issue as i am working with my object, and that
is the idea of things that are build once, and things that need to be re-build
on a change.

Sat 27 Apr 2019 14:36:51 ACST
-----------------------------
the update merge pattern used in the official thins doesnt quite translate for
my things, I think i need another look at how its constructed.


Mon 29 Apr 2019 16:43:41 ACST
-----------------------------
Slowly I am getting the hang of the general update pattern of d3, and its merge
function, it was a bit of a struggle to make happen. but i finally got
directory traversal going. its not that robust but its something.

Mon 06 May 2019 11:31:17 ACST
-----------------------------
ok the next thing i want to make happen is to make the date text pretty, as its
hard to read at the moment. then i want to work on sorting by columns.



Wed 08 May 2019 09:38:50 ACST
-----------------------------
i want to start tackling icons and thumbnails, i think icons will be easier as
they can be based on the mime type of the object in question, whereas the
thumbnails will need to be generated per document, and depending on the
document depends on what happens..so icons first. lets find an opens source
icon set to use, my personal preference is to use the el capitaine theme.. lets
have a look at that first.

it might also be useful to load up local files? or at least use the same
methods to find the icons as normal desktop use on linux and use the same file
structure.

lets check out el-capitaine first. ok icons work :)

Thu 09 May 2019 15:16:35 ACST
-----------------------------
So now i have converted all the blogger entries into plain html( still need to
download the images to be local), i have to analyse them, but my analysis
scripts are borked

i have two, analyse and analyse_all, so i started working on anlayse_all and
realised that its basically an older copy of analyse which had more than the
kitchen sink in it.. so i want to diverge them enough that analyse takes a
single file and spits out the metadata entry for it, and analyse_all
recursively walks the list of files in the content directory but i mean why
would i do that rather than just use a shell command to do it.

for instance,
```bash
for i in *.html; do analyse.py $i; done
```
then use the collate script to join them all together.
I guess a linter might be nice, to find problems with the metadata entries,
which tests for things and i can add more tests as time goes on.. i think i
will scrap the old analyse_all script and create a linter instead which shows
you the problems so they can be fixed.

ok so the analyse.py script has ben tweeaked a little, i want to get some
information for my html documents but i'm unsure what.
whats the kind of things we might like to know about our html before we load
it?
* contains javascript?
* has images?
* lines?
* words?
* direct URL?

I'm also unhappy with the current metadata entries that exist, so i think i
will have to give them a tweak

I also want to analyse the directories more
* contents total size
* last updated file date in the date_time part

also thinking about how to make the description show up nicer, a tooltip like
hover over entries to show their full text, that way there can be more inside
the description without the cutoff being a problem.. not sure how to deal
withthat on mobile. maybe have it as a separate link since there is no on hover
actions.

almost time to go to heaps good dev beers and try to socalise with other
programmers.

Sat 14 Sep 2019 12:50:05 ACST
-----------------------------
OK so i havent touched this for a while, the detailed listing is rudimentary
but it works afaik, it needs a lot of work to make nice. but it works.

something thats not happening is publishing of any works, whether thats images,
or text, i'm not sending anything out into the universe.. so perhaps I can
focus on removing the barriers to entry for publishing data.

One thing i like to do a lot is take macro photo's, and it would be nice if i
could publish that easily.

lets see what the current process is, because i'm completly lost in this
project.

ok, wow this is really not intuitive or easy to pick back up. most of the work
appears to have been spent in the file analysis scripts, i think the purpose
was so that i just drop in files to the folders and then run the analysis and
collate scripts to populate the contents.

I think a service which watches the folders and runs the analysis scripts on
changes would be most practical to remove effort from the user, but also its a
bit weird. even with images there are some things that i would want to modify
on the fly. removing location data, and other metadata for instance.

for photographs, diary and other items, my idea about publishing would be more
like using caja's scripts menu to right click and select publish-image from the
list, which performs all the conversion, places it into the correct directory,
etc.

Wed 20 Nov 2019 12:42:33 ACDT
-----------------------------
OK trying to pick this back up again, I will need to review the way that i have
made the hyperion website because i started working with modules to make things
work and i woud like to continue that here.

the d3 library is missing ebcause the link to the lib direcory was broken..
thats easy enough to fix. [DONE]

ok what scripts exist? i dunno,, what aliases do i have.. none
any symlinks to the scripts? there are some, to the binaries in the folder, but
i really dont know what they do.

i'm so lost on this project, and i dont feel like i can get into it sitting
here.

Thu 21 Nov 2019 08:30:13 ACDT
-----------------------------
lets see if there are any instructions i left for myself

OK the readme does give a general overview which is nice, but it doesnt say
anything about the creation of content. it talks about publishing already
creted content. and for photos that makes sense, but for the general blog post
type thing it doesnt at all

There are a couple of ways i want to do creation but it stems from my diary
such that i load up a terminal and type de. of course i cant do that exactly..
so something like ce or we bp, etc. a very fast two key combination to open an
editor, which when exited will ask you whether you want to publish or you want
to save in a drafts folder. not all things should be published immediately.

how to review those drafts and then publish? i dont know yet.

but the types of things i want to create to be posted are
* markdown thankyou messages to other publishers
* markdown blog posts of raw text

and thats all for now, perhaps as i get used to this type of thing then i might
be able to increase the functionality to work.

if i was to open a terminal and type ty, that works easily
for the blog post i can use either ce, or we

i like the two finger combination with the left hand and then an enter with the
right

OK so since its web index, lets go the we for [w]eb-index [e]ntry

lets get coding.. blahhh..

OK so the alias being short is fine, but the creation script could be all in
one, generic creation script with flags for what type of thing is being
created, then i can alias the short commands to the create withflags.
    eg. alias we create.py -markdown


Fri 22 Nov 2019 12:35:04 ACDT
-----------------------------
After adding more information to the create script i wish now to analyse the
file using the current script not using another one, so i want to import the
analyse script and use its functions directly, which makes me want to do
thaatas the default, and have the analyse functionality.. i wonder if i can
have both, make it a command line application aswell as a library.

OK trying to use it as a library doesnt work it runs the whole thing adding all
its options to the mix,, so i have to separate it out into its own library.

SO what i'm going to do is have
* analyse.py as a module to import
* analyse-cmd.py as a tool to use the analyse module.

i might continue to use that format for other things that need to be
modularised.

Sat 23 Nov 2019 14:27:23 ACDT
-----------------------------
Working on his again with the creation script, and its easy enough to make
work, ,but it makes me think about what sort of things i want to show in the
view.. for objects like tweets, and then how that will effect the rest of the
page..

currently i'm using name and description..but i dont think thats globally
useful.

more like

* Title     - some catchy bullshit
* Subtitle  - generic title
* Summary   - Tweet Sized text
* category  - tags for subject matter

so it could look like:
'''json
{
    title:      "",
    subtitle:   "",
    summary:    "",
    category:   ""
'''

I'm unhappy with how this is progressing but i need to find something that
applies to both long posts, short posts, thanks, pictures, videos etc..

maybe i'm working too hard on this, i need to get back to getting things up
rather than making them easy..

Sun 15 Dec 2019 08:00:02 ACDT
-----------------------------
What was i working on last? the creation script.. which is all fine and dandy.
I can create an obejct and have it analysed after the fact, which then creates
the metadata.. but then it just sits in the scratch folder because there isnt
anything to do after that.

I need to know how to publish the object. which means i need to remember the
structure of the content directory and how it all fits together

Ok so it appears that the object and the json are added to the content-root
folder before being collated into the big database.

so i should be able to perform a straight copy, then database rebuild.

OK so copying the files works ok, but when i go to collate i realise that the
collate object is not a function but a script.. i need to modify it so i can
import it into other places and collate on the fly.

plus i want it to work on a path.. so i can use it on multiple paths and have
it just work within the current directory. since the database will be multiple
layers deep.

the collate function takes all the json data from the current folder, plus or
minus the folders? i could re-analyse the folders, or generate the metadata
individually.. yeah so its rather simple, just collate from the current
directory and dump into one large metadata file..

I think having a separate collate function to operate on the file will be
useful. but ... i know i can make a python script either an import or a script
somehow.. i guess i will have to find out how that works for this to be robust.

ok so the whole content-root and content subdir thing is super awkward and not
intuitive, i'm struggling to remember how i have defined these things.. both in
the code and elsewhere..

So looks like that the metadata files are relative paths..

OK so it works a little.

But some of the cleanup scripts I want dont exist.. like a collate function for
the folder. i removed that in favour of having a simple import statement.. but
i really would like it back.

OK figured out how to make a script differentiated to a module. by using the

```python
if __name__ == '__main__'
```

idiom

OK so i can use that for the analyse tool aswell

I will call it from this moment because i need to commit something that works
for a change.

Sun 15 Dec 2019 20:45:46 ACDT
-----------------------------
is there a todo list in  this project?

Wed 18 Dec 2019 11:40:42 ACDT
-----------------------------
Working on this has been rather stressful as the state that it was in was a
mess.. lots of redundancy and silliness due to it being an immature project.

i have since formalised a lot more of the structure and how it should work so i
think i should be able to proceed. but its still very confusing in my mind on
how to test that it works.

I guess the thing i should make happen is

### Create a post
Use the tool to create a document and have it pushed to the public version of
the website.

### Publish A document
Use the tools the publish an existing document like an image

in this way i can then expand it to
* publish a folder
* update a document
 but in an as needed way.

i can then work on presentation once the publishing is working.

OK so lets crete an publish a test document

Local copy of the website works. now we need to deploy to the web

So it appears that the deploy script still works. but i want to modularise it.
and also that means that it needs to be configured using the configuration
items.

infact i think there is a lot of hard coded junk in my project.. but i'll deal
with that later.

OK im modularising the deploy script and i'm having trouble structuring it..
infact its sort of a wider problem due to the fact tha i have centralise
configurationt that should be able to be used in the main project.. perhaps i
can add verbose to the configuration file, sort of create a default config
object and use it everywhere? overwriting.. hmm.

if its a command line then it needs to load the config object, otherwise it
should expect that the config object already exists, but the deal is that the
config object doesnt exist till its been run from a command line somewhere.


if the config exists then use it..

if it doesnt then we are in an invalid state.

are we running it from the command line?
    load the config

are we importing it?
    config should exist already

OK so python control flow doesnt create a variable scope so that makes things a
little easier for this type of thing. so we can get the arguments, or have the
arguments for verbose always available if its imported from another script that
creates those.. its part of a whole.. its a bit weird, i like to have stand
alone things but this is a singular project so it doesnt cause much trouble to
have this type of thing. perhaps if we are not main and no variable exists we
can define defaults for the individual scripts so that they still work in some
off chance

So deploy works on its own, now how about as a module.

A new problem arises,  the namespacing of modules prevents me from using a
global configuration dictionary without something.. so what is that something?
looks too hard, i have to think about this some more.

OK success for basic text entries, the creation pipeline goes from opening up a
text editor to publishing it to the web..

Now i need to get other things published like folders of documents, images etc.

Sat 21 Dec 2019 11:45:30 ACDT
-----------------------------
So the next task is to make it possible to publish things from caja.
firstly i need to get the script thing working. i found a way to extend the
python path so that it can pick up local scripts so i can test this prior to
making it an installable module.

i also decided that tkinter is the toolkit to use based on the fact that its
inbuilt for python and my needs are rather simple. otherwise pyqt5 would be the
way to go.

ideally there would be a confirmation dialog in which you might be able to
specify a folder, or use the default folder and a cancel button.

the actions would be extremely similar to the creation script.

such that it
* Make a copy of the file and put it in the scratch folder
* Analyse the file and generate the metadata
* provide an option to review the metadata, and edit it.
* copy the files to the final location
* rebuild the metadata database
* deploy to the web.

due to the fact that create and publish are so similar it might be worth
joining their functionality together..

what are the contexts to make this happen?
* cmdline
* gui.

so it might be worth having a --gui switch in the script, specify a input file,
or if left out jump into creation mode.

what might that look like?

* we --gui
    - perform a file creation and review using gui tools
* we --gui filename
    - publish existing file using gui tools
* we filename
    - publish an existing file
* we
    - create and publish using commandline tools.


since the latter half of the script is the same thing in both cases it makes
sense to me to make it this way..

the caja script can simply use this tool in gui mode, and pass it the files
specified in the environment variables.

Only thing left to decide is whether to copy the creation script or start
again.

I think i should copy and rename.. so what do i call it?


Sun 22 Dec 2019 09:58:33 ACDT
-----------------------------
so like i stated earlier, what are the ways that the script can be invoked.

* as an import
* as a script
    if __name__ == '__main__':

depending on options given, then it will run the gui or not.. my mind isnt very
organised on this matter.

it also needs to take either no optional arguments(create)
or one to many optional arguments

Thu 26 Dec 2019 09:56:25 ACDT
-----------------------------
So i've been evaluating tkinter for the toolkit as it comes bundled with python
installations.. which means its always available. but its options for use are
fairly limited, I will have to make my interface options limited also.

what is it that i wish to display to the user?

I think firstly an OK/Cancel box asking whether to publish the selected item.

but that OK/Cancel dialog doesnt need to be simple, it can be complex, with all
the information needed to make it happen.

so OK/Cancel

I want to select the folder that it goes into, but perhaps since the original
script doesnt have this i can leave it out for now and jyst stick with the
OK.cancel box.

The problem i am having is that to get  GUI running it constitutes a new
learning paradigm, and i dont want to learn something that cannot be extended
well into the future.

so i think i might drop tkinter and use pyqt5 so that i have some forward
looking prospectives.


blahh.. ok so trying to install pyqt5 or use its examples is being a pain
but i'll get these examples somehow, there are unofficial clones of the source
on github, but i want the originals, no someone elses.. i prefer to get from
the source rather than some clone.

so this ends up being the way to to get the software:
https://www.riverbankcomputing.com/software/pyqt/download5

which sucks since i am used to source repos all the time.

OK so i have downloaded the examples, and upon first inspection i am calmed,
which means its the right direction to head.

however making the app dependent on pyqt5 to operate seems a bit overkill.. is
there a way that i can have my cake and eat it too without doubling up on the
scripting madness?

OK i guess i have to ask myself whether having a GUI application for this is
worth the trouble.. cant i just use the command line and be done with it? move
onto more important things.. i guess it was the desire to right click and
publish that pushes this whole thing.. but i can operate on files just aswell
in the command line as i can in the browser.. and a complimentary terminal and
file manager does the job as good, even if its not as simple as right click
publish..

i can always revisit to make it work in a gui at a later date.

yes, i think skipping this for now is the most appropraite option.. qt5 is
definitely something i would like to achieve in the future, but with the way my
brain works, it complicates things immensely.


Removed reference to publish in the caja scripts folder.

Fri 27 Dec 2019 10:59:04 ACDT
-----------------------------
OK so continuing to work on the create/publish script and i want to be able to
publish into  sub folders, however that raises the issue that for every depth
level of a subfolder i need to re-analyse the contents and update its metadata
on a change.

so it becomes a sort of reverse recusive update of the subfolders once all th
copy operations are performed. and that happens at the very end of the script..

but doing it for each file upload in an externally recursive script is such a
waste of time.

It might be a better idea to have that separate? and can it only count the json
files rather than the contents? the numebers will be off since i'm storing the
metadata myself in the folders.

Perhaps it might be better to store the metadata separately? though its nice to
have it in the same folder accompanying the data.. trade offs.

OK so it works for one folder deep, but there is no indication about which
folder we are in, its name or anything.. which means the folder analysis is
broken

now i want the images to be loaded in place and be able to scrub through them
on the website, which means creating a content overlay that shifts to the next
entry. should be overlay hard, but its interesting to see how things are
shaping up.

it makes me think that i need to have the metadata also saved back into the
file, exif and all that

and also that i need to somehow upate the URL on a directory change or use
javascript to overcome the back button. either way the publish to subfolder is
working, an di need to move onto other things.

perhaps i need to use it a little and see what shows up asthe primary blocker.

I could simply turn off the directory listing, and then its a guess as to what
the URL's are.
