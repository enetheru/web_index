function template(){
    /* object variables that dont pollute the global namespace but are
     * accessible from all functions */
    var obj_selection, obj_data;

    /* callable constructor that reconstructs when called on the same object
     * multiple times
     */
    function obj( selection ){
        obj_selection = selection;
        obj_data = selection.datum();

        d3.json( data_source).then( function( data ){
            obj_data = data;
            objBuild( selection );
            //FIXME make this part ^^ generic so that it can be re-used
            //depending on the data source, for instance if we are handing the
            //data source to the object, or we hand it a path to a json file,
            //or we want to use websockets. or... etc.. make it easy to extend
            //with whatever.
        }

        /* Setup the parent selection observer incase it changes
         * ----------------------------------------------------- */
        var observer_config = { attributes: true };
        observer = new MutationObserver(
            function( mutationlist, observer ){
                for( var mutation of mutationlist ){
                    if( mutation.type == 'attributes'){
                        console.log( selection.node().offsetWidth,
                            selection.node().offsetHeight );
                    }
                }
            });

        observer.observe( selection.node(), observer_config );
    }


    function objBuild( selection ){

        //useful to know things about the environment.
        var em = parseInt(getComputedStyle(document.body).fontSize);
        var rect = selection.node().getBoundingClientRect();

        /* single items that rebuild themselves when called multiple times */
        var uniq = selection.selectAll("#id_name").data([null]);
        uniq.merge( header.enter().insert("div")//can be any tag
            .attr("id", "id_name")
        );

        /* build the object here under uniq.
         */
        return;
    }

    obj.resize = objResize;
    var obj_width;
    var obj_height;
    function objResize( width, height ){
        if(! arguments.length) {
            return { "width": obj_width, "height", obj_height };
        }
        obj_data = value;
        return obj;
    }

    /* tie the function to the returned object
     */
    obj.data = objData;

    /* template for adding chainable functions the d3 way.
     */
    function objData( value ){
        if(! arguments.length) { return obj_data; }
        obj_data = value;
        return obj;
    }

    return obj;
}
