/*
 * This document is for the code of creating a tabular view.i
 *
 * DONE - Separate the table header from the body so that it ats like a freeze
 * row. I will have to style the table body such that you realise that there
 * is more data below and above. aka fixed header scrolling
 * https://stackoverflow.com/questions/8423768/freeze-the-top-row-for-an-html-table-only-fixed-table-header-scrolling#8423803
 *https://developers.google.com/web/updates/2012/08/Stick-your-landings-position-sticky-lands-in-WebKit
 *
 * TODO - adding column headers
 * TODO - optional header column for row number
 *
 * Drag Operations
 * TODO - header order re-arragement
 * TODO - column resizing
 * TODO - capture mouse to allow dragging outside of window bounds.
 *
 * OnClick Operations
 * TODO - Column Sorting
 * TODO - Column Deleting
 * TODO - enable word wrapping on column
 *
 * A new alt on the ability to add columns headers, make the column headers
 * editable, with auto complete, and enable js function in the header item..
 * if there are comma's then add additional columns to fil lout the columns.
 *
 * fade in and out icons on hover rather than have them always there.
 */

function tabular(){
    var obj_selection, obj_data;
    var obj_croot = "content/";
    var width, height, em;
    var column_data = [
        { name:"date_time"  },
        { name:"category"   },
        { name:"icon"       },
        { name:"name"       },
        { name:"description"}
    ];

    /*temp shit*/
    var x_scale;
    var colours = ['red','blue','green','yellow','cyan','purple']

    function obj( selection ){
        obj_selection = selection;

        d3.json(obj_path + "db.json").then( function( data ){
            obj_data = data.sort(function(a,b){
                //sort undefined things to the top
                if( a.date_time == undefined ) return -1;
                if( b.date_time == undefined ) return 1;
                // else in date order from newest to oldest
                return new Date(Date.parse(b.date_time))
                    - new Date(Date.parse(a.date_time));
            });
            objBuild(selection);
            objResize( obj_width, obj_height );
        });


        /* Setup the parent selection observer incase it changes
         * ----------------------------------------------------- */
        var observer_config = { attributes: true };
        observer = new MutationObserver(
            function( mutationlist, observer ){
                for( var mutation of mutationlist ){
                    if( mutation.type == 'attributes'){
                        objResize( selection.node().offsetWidth,
                            selection.node().offsetHeight );
                    }
                }
            });

        observer.observe( selection.node(), observer_config );
        return;
    }

    function objBuild( selection ){
        //obj_selection = selection;
        //obj_data = selection.datum();
        //
        /*
         * There are a number of items that remain no matter the contents
         * <g id="display">
         *     <g id="header">
         *     <g id="rows">
         *
         * these items do not change, and merely represent groupings to better
         * orient the contents, but are they needed? I think its nice to have
         * for interface hooks, style choices etc.
         *
         * how do i make them so that they are never rebuilt a second time?
         * they are also nested within each other, so its a layered situation.
         */

        /*var svg = selection.selectAll("svg").data([null])
        svg.merge( svg.enter().append("svg"));

        var display = svg.selectAll("#display").data([null]).enter()
          .append("g").attr("id", "display");

        display.append("g").attr("id", "header");
        display.append("g").attr("id", "rows")
            .on("click", function() {
                console.log("clicked");
                objBuild( selection );
            })
          .append("rect")
            .attr("width", 100)
            .attr("height", 100);



        return;
       */
        em = parseInt(getComputedStyle(document.body).fontSize);

        // Set the svg width to full size
        obj_width = d3.select("#viewbox").node().getBoundingClientRect().width;
        obj_height = em * 1.5 * (obj_data.length +1);

        // TODO calculate optimal column widths.
        // for now we will just evenly split the available space
        /* FIXME blocked out temporarily because its done in the resize
         * function
        var column_width = (obj_width / column_data.length)
        var column_pos = 0;
        for( var c in column_data ){
            if( column_data[c].name == "icon" ){
                console.log( column_data[c] );
                continue;
            }
            column_data[c].x = column_pos;
            column_data[c].width = column_width;
            column_pos += column_width;
        }*/

        /* top level SVG tag */
        var svg = selection.selectAll("#view").data([null]);
        svg.merge( svg.enter().insert("svg")
            .attr("id", "view")
            .attr("class","tabular")
            .attr("xmlns" ,"http://www.w3.org/2000/svg")
        );


        /* DISPLAY LAYER */
        var g_display = svg.selectAll("#display").data([null]).enter()
          .append("g")
            .attr("id", "display");

        // generate headers
        var header = g_display.append("g")
            .attr("id", "header")
            .attr("class", "tabular");

        header.selectAll("g")
            .data(column_data)
            .enter()
          .append("g")
            .attr("transform", function(d,i){ return `translate(${d.x},0)`; })
            .each( function(d,i){
                var col = d3.select(this);
                col.append("rect")
                    .attr("width", d.width )
                    .attr("height", em + em / 2 )
                if( d.name != 'icon' ){
                    var text = col.append("text")
                        .text( d.name )
                        .attr("transform", `translate(${em/2},${em})`);
                }
            });

        // generate rows
        var rows = g_display.append("g")
            .attr("id", "rows")
            .attr("transform", `translate(0,${em * 1.5})`);

        var s_rows = d3.select("#rows").selectAll("#row").data( obj_data );

        // append rows
        s_rows.enter()
          .append("g")
            .attr("id", "row")
            .attr("class", "row")
            .attr("transform", function(d,i){
                return `translate(0,${(em / 2) * 3 * i})`;
            })
        //Update rows
          .merge(s_rows)
            .each(function(d,i){
                // transform data for each cell.
                row_data = [];
                for( c in column_data ){
                    var key = column_data[c].name;
                    if( key == "date_time" ){
                        var options = { year: 'numeric', month: 'numeric', day: 'numeric'};
                        var date = new Date( Date.parse( d.date_time ));
                        row_data[c] = date.toLocaleDateString('default', options).replace(/\//g,'-');
                        continue;
                    }
                    if( key == "icon" ){
                        if( d.mime == undefined ){
                        } else if( d.mime == 'inode/directory'){
                            row_data[c] = "icons/default/places/scalable/folder.svg";
                        } else if( d.mime == "null/null"){
                            row_data[c] = "icons/default/mimetypes/scalable/empty.svg";
                        } else {
                            row_data[c] = "icons/default/mimetypes/scalable/" + d.mime.replace(/\//g, '-') + '.svg';
                        }
                        continue;
                    }
                    row_data[c] = d[key];
                }

                var s_cells = d3.select(this).selectAll(".cell").data( row_data );
                //Append Cells
                s_cells.enter().append("g")
                    .attr("id", function(d,j){ return `${j},${i}`} )
                    .attr("class", "cell")
                    .attr("transform", `translate(${column_data[c].x},0)`)
                  .each( function(cell_data,i){
                    var cell = d3.select(this);

                    cell.append("rect")
                        .attr("width", column_data[c].width )
                        .attr("height", em * 1.5 )

                    // after the rect, put inside it custom things depending on
                      // what we are looking at.
                      if( column_data[i].name == 'icon' ){
                          cell.append("image")
                              .attr("height", em * 1.5)
                              .attr("xlink:xlink:href", row_data[i] );
                          return;
                      }

                    // remaining automtic things.
                    cell.append("text")
                        .attr("transform", `translate(${em/2},${em})`)
                        .text(cell_data);
                  })
                //Update Cells
                  .merge( s_cells )
                    .each( function(d,i){
                        var cell = d3.select(this);
                        if( column_data[i].name == 'icon'){
                            cell.select("image")
                              .attr("xlink:xlink:href", row_data[i]);
                            return;
                        }
                        cell.select("text").text(d);
                    });
                //Remove Cells
                s_cells.exit().remove();
            })
            .on("click", function(d,i) {
                if( d.mime == "inode/directory"){
                    obj_path = "content/" + d.filename + "/";
                    obj(obj_selection);
                    console.log("new path", obj_path);
                } else if( d.filename == undefined) {
                    console.log("Filename", d.filename );
                } else {
                    window.location = obj_path + d.filename;
                }
            });

        //remove rows
        s_rows.exit().remove();

        /* COLUMN CONTROL LAYER */
        var g_cc = svg.selectAll("#column_control").data([null]).enter()
          .append("g")
            .attr("id", "column_control");

        /* ROW CONTROL LAYER */
        var g_rc = svg.selectAll("#row_control").data([null]).enter()
          .append("g")
            .attr("id", "row_control");


        return;
    }

    obj.resize = objResize;
    var obj_width;
    var obj_height;

    function objResize( width, height ){
        if(! arguments.length) {
            return { "width": obj_width, "height": obj_height };
        }
        obj_width = width;
        obj_height = height;

        var svg = obj_selection.select("svg")
            .attr("width", obj_width )
            .attr("height", obj_height );

        /* first pass to account for fixd width columns like 'icon'
         * this sets the width, and the size attributes */
        var r_width = obj_width;
        var r_count = column_data.length;
        for( var c in column_data ){
            // set fixed size for icon column
            // FIXME this could be moved into a fixed area above rather than
            // calculated on every resize;
            if( column_data[c].name == "icon" ){
                column_data[c].width = em *1.5;
                column_data[c].size = 'fixed';
            }

            // remove fixed column sizes from remaining width
            if( column_data[c].size == 'fixed' ){
                r_width -= column_data[c].width;
                r_count--;
                continue;
            }

            // remaining columns are auto sized
            if( column_data[c].size == undefined ){
                column_data[c].size = 'auto';
                continue;
            }

        }
        /* Second pass to re-size and position all the columns based on their
         * fixed sizes or use the remaining automatic space */
        var column_width = Math.max((r_width / r_count),1);
        var column_pos = 0;
        for( var c in column_data ){
            column_data[c].x = column_pos;

            // move the next x coordinate by width of the object
            if( column_data[c].size == 'fixed' ){
                column_pos += column_data[c].width;
                continue;
            }

            column_data[c].width = column_width;
            column_pos += column_width;
        }

        // resize the headers
        svg.select("#header").selectAll("g").each( function(){
            var group = d3.select(this);
            var data = d3.select(this).datum();
            group.attr("transform", `translate(${data.x},0)`);
            group.select("rect").attr("width", data.width );
        });

        // resize the columns.
        d3.selectAll("#rows").selectAll(".cell").each( function(){
            var col = parseInt( d3.select(this).attr("id")[0] );
            var group = d3.select(this);
            group.attr("transform", `translate(${column_data[col].x},0)`);
            group.select("rect").attr("width", column_data[col].width );
        });
        return obj;
    }


    /* setter and getter chain template */
//    var obj_var;
//    obj.funcTemplate = objFuncTemplate;
//    function objFuncTemplate( value ){
//        if(! arguments.length) { return obj_var; }
//        obj_var = value;
//        return obj;
//    }


    var obj_path = "";
    obj.path = objPath;
    function objPath( value ){
        if(! arguments.length) { return obj_path; }
        obj_path = value;
        return obj;
    }


    return obj;
}
