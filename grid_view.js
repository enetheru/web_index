/*
 * This script is to separate out the variable namespace of things internal to
 * the grid view and from th encasing html file. ideally it will make things
 * simpler in the long run
 *
 */

var grid_obj = function() {
    var obj_selection,obj_data;
    var obj_index, obj_width = 'auto', obj_height = 'auto';

    function obj( selection ){
        obj_selection = selection;
        obj_data = selection.data();

        console.log(selection.data());
        selection.append("div")
            .text( function(d,i){ return d.name;} );
    }

    obj.data = objData;
    obj.index = objIndex;
    obj.width = objWidth;
    obj.height = objHeight;

    function objData( value ){
        if(! arguments.length) { return obj_data; }
        obj_data = value;
        return obj;
    }

    function objIndex( value ){
        if(! arguments.length) { return obj_index; }
        obj_index = value;
        return obj;
    }

    function objWidth( value ){
        if(! arguments.length) { return obj_width; }
        obj_width = value;
        return obj;
    }

    function objHeight( value ){
        if(! arguments.length) { return obj_height; }
        obj_height = value;
        return obj;
    }


    return obj;
}

var grid_obj_detail = function() {
    var obj_selection,obj_data;
    var obj_index, obj_width = 'auto', obj_height = 'auto';

    function obj( selection ){

        obj_selection = selection;
        obj_data = selection.data();
        selection.attr("style", "white-space: nowrap");

        for( item in obj_data[0] ){
            selection.append("span")
                .attr( "style", "margin: 3px")
                .text( item + ":" + obj_data[0][item] );
        }
    }

    obj.data = objData;
    obj.index = objIndex;
    obj.width = objWidth;
    obj.height = objHeight;

    function objData( value ){
        if(! arguments.length) { return obj_data; }
        obj_data = value;
        return obj;
    }

    function objIndex( value ){
        if(! arguments.length) { return obj_index; }
        obj_index = value;
        return obj;
    }

    function objWidth( value ){
        if(! arguments.length) { return obj_width; }
        obj_width = value;
        return obj;
    }

    function objHeight( value ){
        if(! arguments.length) { return obj_height; }
        obj_height = value;
        return obj;
    }


    return obj;
}

var grid_view = function() {
    var view_selection, view_data;
    /* rows and columns, specifies how many cells exist in the grid view,
     * its hard to think about how to specify ths, because if both are auto
     * then how do you build the interface? i want it to flow like a file
     * manager, which changed depending on what style of view it is, a detailed
     * list only has one columns, and a list view breaks at the page height.
     *
     * detailed View: rows = infinite  cols = 1
     * list view:     rows = to fit    cols = infinite
     * icon view:     rows = infinite  cols = to fit
     *
     * lets enumerate all three possibilities
     * rows = infinite, fit, value
     * cols = infinite, fit, value
     *
     * inf, inf - requires ratio, but all other combinations are OK.
     * inf, fit - flows vertically, fits horizontally
     * inf, val - flows vertically, limited horizontally
     * fit, inf - fits height, flows horizontally.
     * fit, fit
     * fit, val
     * val, inf
     * val, fit
     * val, val
     * I think i might switch my thinking to go columns first, and rows second
     *
     * there are two directions we can build the list horizontally and
     * vertically. depending on which column is set to inf, will change the
     * flow direction, except where both are set to inf, in which case i might
     * make it throw an error, or if none are set to inf, its weird to think
     * about. also what happens when we run out of space, i want to put a
     * little thing there to make more. but how do i know ahead of time how
     * many fit? i guess i will have to calculate it and filter the list.
     */
    var cols = 1, rows = 1;
    var col_width = 128, row_height = "100%";
    var grid_flow = 'row'; //row,column
    var style;

    /* local data */
    function view( selection ){
        view_selection = selection;
        view_data = selection.datum();

        var grid = selection.selectAll("#grid").data([null]);

        grid.merge( grid.enter().insert("div")
            .attr("id", "grid")
            .attr("class","grid"));

        // Find the stylesheet and attach it for modification later.
        // at some point it would be nice to create a unique one for this
        // object ad modify it as necessary
        // FIXME do the above.
        for( var i = 0; i < document.styleSheets.length; ++i ){
            for( var j = 0; j < document.styleSheets[i].cssRules.length; ++j ){
                var cssRule = document.styleSheets[i].cssRules[j];
                if( cssRule.selectorText == ".grid"){
                    style = cssRule.style;
                }
            }
        }
        style.gridAutoFlow = grid_flow;

        //columns can be either a value or 'fit'
        style.gridAutoColumns = col_width;
        if( typeof cols === 'number' && (cols % 1) === 0 ){
            style.gridTemplateColumns = (col_width + " ").repeat(cols);
        }
        else if( cols == 'fit' ){
            cols = Math.floor( window.innerWidth / parseInt(col_width) );
            if( cols == 0) cols == 1;
            style.gridTemplateColumns = (col_width + " ").repeat(cols);
        }

        //rows can be either a value or 'fit'
        style.gridAutoRows = row_height;
        if( typeof rows === 'number' && (rows % 1) === 0 ){
            style.gridTemplateRows = (row_height + " ").repeat(rows);

        }
        else if( rows == 'fit' ){
            rows = Math.floor( window.innerHeight / parseInt(row_height) );
            if( rows == 0) rows == 1;
            style.gridTemplateRows = (row_height + " ").repeat(rows);
        }

        /* build table rows */
        var cells = grid.selectAll("div")
            .data( view_data )
            .enter()
          .append("div")
            .attr("class", "grid-area");

        var content = grid_obj_detail();

        cells.each( function(d, i){
            //console.log(i,d);
            obj = d3.select(this);
            obj.call( content );
            return;
        });
    }

    function recalCols(){
        /*
         * Situation
         * both are auto
         * both are set
         * cols is auto
         * col_width is auto
         */

        /* simple case both are set */
        console.log( cols, ", ", col_width );
        if( cols > 0 && col_width > 0 ){
            /* FIXME depending on the size of the information in the boxes */
            style.gridTemplateColumns = (col_width + "px ").repeat(cols);
            style.gridAutoColumns = col_width + "px";
            return;
        }
        /* simple case both are auto */
        if( cols < 1 && col_width < 1 ){
            style.gridTemplateColumns = null;
            style.gridAutoColumns = "100%";
            return;
        }
        if( cols < 1 ){
            console.log("auto columns");
            var tcols = Math.floor( window.innerWidth / parseInt(col_width) );
            console.log(tcols);
            if( tcols < 1 || tcols == Infinity) tcols = 1;
            style.gridTemplateColumns = ((col_width ? col_width + "px " : "100%")).repeat(tcols);
            style.gridAutoColumns = col_width + "px";
            return;
        }
        if( col_width < 1 ){
            // FIXME pull width of grid item
            var twidth = Math.floor( window.innerWidth / cols );
            console.log( twidth );
            style.gridTemplateColumns = ((twidth ? twidth + "px " : "100%")).repeat(cols);
            style.gridAutoColumns = twidth + "px";
            
            return;
        }
    }

    function recalRows(){
        /*
         * Situation
         * both are auto
         * both are set
         * cols is auto
         * col_width is auto
         */

        /* simple case both are set */
        console.log( rows, ", ", row_height );
        if( rows > 0 && row_height > 0 ){
            /* FIXME depending on the size of the information in the boxes */
            style.gridTemplateRows = (row_height + "px ").repeat(rows);
            style.gridAutoRows = row_height + "px";
            return;
        }
        /* simple case both are auto */
        if( rows < 1 && row_height < 1 ){
            style.gridTemplateRows = null;
            style.gridAutoRows = "100%";
            return;
        }
        if( rows < 1 ){
            console.log("auto rows");
            var trows = Math.floor( window.innerHeight / parseInt(row_height) );
            console.log(trows);
            if( trows < 1 || trows == Infinity) trows = 1;
            style.gridTemplateRows = ((row_height ? row_height + "px " : "100%")).repeat(trows);
            style.gridAutoRows = row_height + "px";
            return;
        }
        if( row_height < 1 ){
            // FIXME pull width of grid item
            var theight = Math.floor( window.innerHeight / rows );
            console.log( theight );
            style.gridTemplateRows = ((theight ? theight + "px " : "100%")).repeat(rows);
            style.gridAutoRows = theight + "px";
            
            return;
        }
    }


    view.cols = viewCols;
    view.colWidth = viewColWidth;
    view.rows = viewRows;
    view.rowHeight = viewRowHeight;
    view.gridFlow = viewGridFlow;
    view.dir = viewDir;

    function viewCols( value ){
        if(! arguments.length) { return cols; }
        cols = parseInt( value );
        recalCols();
        return view;
    }

    function viewColWidth( value ){
        if(! arguments.length) { return col_width; }
        col_width = parseInt(value);
        recalCols();
        return view;
    }

    function viewRows( value ){
        if(! arguments.length) { return rows; }
        rows = parseInt(value);
        recalRows();
        return view;
    }

    function viewRowHeight( value ){
        if(! arguments.length) { return row_height; }
        row_height = parseInt(value);
        recalRows();
        return view;
    }

    function viewGridFlow( value ){
        if(! arguments.length) { return grid_flow; }
        grid_flow = value;
        style.gridAutoFlow = grid_flow;
        return view;
    }

    function viewDir( value ){
        /* takes value of 'column' or 'row' */
        if(! arguments.length ){ return dir; }
        return view;
    }

    return view;
}
