#!/usr/bin/env python

import argparse
import os
import shutil
from pprint import pprint
from pprint import pformat
from datetime import datetime
import subprocess
import json

from config import Config
from collate import collate
from analyse import analyse
import deploy

desc = """web-entry.py is used to creeate and or publish document to the
website."""

"""NOTES:
    this script is to create and or publish a document to the site, it operates
    on a single file, so multiple files can be done using shell for loops.
    """

if __name__ == '__main__':
    #perform things because we are used directly.
    # Not sure if there is anything we might want to import at this stage
    print("invoked from the command line as the main script")

# Templates
# ---------
# This might want an overhaul to make more robust, optional, customisable etc.
# for instance, pulling from a separate subfolder
tweet_template = "TITLE\n\nShort Content"
markdown_template = """TITLE
-----
Long Content
"""
thankyou_template = """A Big Thank-You to <insert persons name here>
For?
Links?"""


# Argument Parser
# ---------------
parser = argparse.ArgumentParser(description=desc)

parser.add_argument( '-template', choices=['markdown','tweet','thankyou'],
        default='post',
        help='Specify what template to use when populating content')

parser.add_argument('-config',nargs='?',
        help="configuration to load")

parser.add_argument( 'file', nargs='?',
        help="files to publish" )

parser.add_argument( 'dest', nargs='?',
        help="folder to publish to" )

args = parser.parse_args()

# Configuration Loading
# ---------------------
cnf = Config( "enetheru" )
if args.config:
    cnf.add_path( args.config )

# Default Configuration
config = {
        'Content-Root':'.',
        'Content-Folder':'content',
        }

# override with user config
config = {**config, **cnf.load() }

#override with site config 
with open(config['Content-Root'] + '/config.json') as f:
    config = {**config, **json.load(f) }

pprint( config )

# File Creation
# -------------
# confirm/generate scratch folder
if config['Scratch-Folder'].startswith('/'):
    # is an absolute path
    scratch_folder = config['Scratch-Folder']
else:
    scratch_folder = '{}/{}'.format(config['Content-Root'],
            config['Scratch-Folder'])
os.makedirs(scratch_folder, exist_ok=True)

if args.file == None:
    # Create file with template
    template = {
            'tweet'     : tweet_template,
            'markdown'  : markdown_template,
            'thankyou'  : thankyou_template
        }.get(args.template, "<name>")

    # set filenames
    filename = '{}-{}.md'.format( args.template,
            datetime.now().strftime('%Y-%m-%dT%H%M%S%Z') )
    filepath = '{}/scratch/{}'.format( config['Content-Root'], filename )


    with open( filepath, 'w') as f:
        f.write( template )
        f.close()

    # FIXME using vim here is arbitrary, allow configuration override or use
    # environment variable.
    process = subprocess.run( ['vim', filepath] )
else:
    # Copy specified file to the scratch folder
    shutil.copy2( args.file, scratch_folder )
    filename = args.file
    filepath = scratch_folder + '/' + filename

# Analyse content
# ===============
print( "ANALYSING", filepath )
metadata = analyse( filepath )

# Fix filename
if 'filename' in metadata:
    metadata['filename'] = os.path.basename( metadata['filename'] )

if 'mime' in metadata and metadata['mime'] == 'text/plain':
    #if tweet sized then use the whole thing as the name
    if metadata['word_count'] < 30 and metadata['line_count'] < 5:
        with open( filepath ) as post:
            metadata['name'] = post.read()
        metadata.pop('filename')
        metadata.pop('mime')
        metadata['category'] = 'tweet'
    else:
        metadata['category'] = 'post'

    # Add Name
    if 'name' not in metadata:
        with open( filepath ) as post:
            metadata['name'] = post.readline(64)

# dump metadata to file
with open( filepath + '.json', 'w') as f:
    f.write( json.dumps( metadata, indent=2 ) )
    f.close()

#ask the user what to do
while True:
    with open( filepath + '.json' ) as f:
        try:
            metadata = json.load( f )
            print("Metadata:" )
            pprint( metadata )
        except:
            print("Failed to decode json")
            f.close()

    option = input( "[e]dit meta-data, [p]ublish, [d]iscard?" )
    # add options to re-edit the file, re-analyse the file
    if option == 'p':
        break;
    if option == 'e':
        process = subprocess.run( ['vim', filepath + '.json'] )
    elif option == 'd':
        os.remove( filepath )
        os.remove( '{}.json'.format( filepath ) )
        exit(1)

# Copy to content folder
# ======================
# Test that the destination folder exists
dest_folder = '{}/{}'.format( config['Content-Root'], config['Content-Folder'] )
if args.dest != None:
    dest_folder += '/' + args.dest

    if not os.path.exists( dest_folder ):
        os.makedirs(dest_folder, exist_ok=True)
        print( "Creating destination folder: " + dest_folder )

if 'filename' in metadata:
    shutil.move( filepath, dest_folder + '/' + filename )
shutil.move( filepath + '.json', dest_folder + '/' + filename + '.json' )

# collate, and update subfolders
# ------------------------------
while os.path.split( dest_folder )[1] != config['Content-Folder']:
    collate( dest_folder )

    with open( dest_folder + '.json', 'w') as f:
        f.write( json.dumps( analyse( dest_folder ), indent=2 ) )
        f.close()

    dest_folder = os.path.split( dest_folder )[0]
collate( dest_folder )

# Deploy to cloud
# ===============
deploy.args = args
deploy.config = config
deploy.deploy()
