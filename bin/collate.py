#!/usr/bin/env python
"""
This script collates the json objects from all the content metadata files into
one large file.

arguments
    --config = specify the configuration file
    --recursive
    [folder] Specify a folder path
"""

#FIXME by default the collate script should use the configuration to
# recursively update the Content-Root/content directory and all subdirectories
import json
import glob
import datetime
import sys, os
import argparse
from pprint import pprint
from config import Config

# Recurse Function
# ================
def recurse( path ):
    os.chdir( path )

    collate( path )

    if not os.path.exists( "db.json" ):
        print("unable to read db.json")
        return;

    dbfile = open("db.json", "r" )
    db = json.load( dbfile )
    dbfile.close()

    for entry in db:
        if not "filename" in entry:
            continue

        if os.path.isdir( entry['filename'] ):
            recurse( os.path.abspath( entry['filename'] ) )
    return

# Collate Function
# ================
# TODO add metadata for the database itself will need a restructure of database
# information
"""
{
    #analysis goes here
    "last_update": datetime,
    "entries":[{},{},...]
}
"""
#database.write("dataset_meta = {\n"
#"\t\"build_date\":\"" + datetime.datetime.now().isoformat() + "\"\n}")
def collate( path ):
    if not os.path.isdir( path ):
        print("[ERROR] invalid Content-Location:", path )
        return False

    os.chdir( path )

    metadb = []
    metafiles = glob.glob( "*.json" )
    if "db.json" in metafiles:
        metafiles.remove("db.json")

    for metafile in metafiles:
        try:
            metadata = json.load( open( metafile ) )
        except:
            print("[ERROR] Failed to load", metafile )

        metadb.append( metadata )

    sitedb = open("db.json", 'w')
    sitedb.write( json.dumps(metadb, indent=2) )
    sitedb.close()

    print( "processed", len( metadb ), "items in", path )

    return metadb


# Script Actions
# ==============
if __name__ == "__main__":
    # execute only if run as a script
    # Argument Parsing
    #=================
    parser = argparse.ArgumentParser(description="collate metadata")

    parser.add_argument( '-c', '--config', default='web-index',
            help='specify name of configuration')

    parser.add_argument('-r', '--recursive', action="store_true",
            default=False,
            help="recursively enter folders")

    parser.add_argument('path', nargs='?',
            help="folder to operate on")

    args = parser.parse_args()
    print( "Args:",  args )

    # Default Configuration
    config = {
            'Content-Root':'.',
            'Content-Folder':'content',
            }

    # override with user config
    config = { **config, **Config( args.config ).load() }

    #override with site config 
    if 'Content-Root' in config:
        with open(config['Content-Root'] + '/config.json') as f:
            config = {**config, **json.load(f) }

    print('Configuration:')
    pprint( config )

    if args.path == None:
        print( "using Content-Root" )
        args.path = config['Content-Root'] + '/' + config['Content-Folder']

    if args.recursive:
        recurse( args.path )
    else:
        collate( args.path )
