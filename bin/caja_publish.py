#!/bin/env python
# All executable files in this folder will appear in the Scripts menu. Choosing a
#   script from the menu will run that script.
# When executed from a local folder, scripts will be passed the selected file
#   names. When executed from a remote folder (e.g. a folder showing web or ftp
#   content), scripts will be passed no parameters.
# In all cases, the following environment variables will be set by Caja, which
#   the scripts may use:
# CAJA_SCRIPT_SELECTED_FILE_PATHS: newline-delimited paths for selected files
#   (only if local)
# CAJA_SCRIPT_SELECTED_URIS: newline-delimited URIs for selected files
# CAJA_SCRIPT_CURRENT_URI: URI for current location
# CAJA_SCRIPT_WINDOW_GEOMETRY: position and size of current window
# CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS: newline-delimited paths for selected
#   files in the inactive pane of a split-view window (only if local)
# CAJA_SCRIPT_NEXT_PANE_SELECTED_URIS: newline-delimited URIs for selected files
#   in the inactive pane of a split-view window
# CAJA_SCRIPT_NEXT_PANE_CURRENT_URI: URI for current location in the inactive
#   pane of a split-view window

# Brainstorm
# ----------
# My ideas for this script are that it needs to have a cancel or OK dialog
# showing information about the action about to be taken.. because caja is a
# file manager, which is a GUI application then the tool needs to be a gui
# application too.
#
# The main thing is that it needs to be simple enough to be right click, press
# the button, and then press enter, or OK. no fluffing around.
# 
# I'm not sure yet if i want much features, perhaps the ability to choose a
# subfolder to publish to.

# Where I am expecting to run into trouble is the importing of my submodules.
# perhaps there is some way to test if it was run from caja, like these
# environment variables above, in which case i could run the tkinter version of
# the tool, and otherwise run the command line version of the tool.

# which means a re-structure of the code again to enable both GUI and command
# line application.

# So i need to have the entire folder available to caja for it to work.^
import os

import tkinter
from tkinter import messagebox

# hide main window
root = tkinter.Tk()
root.withdraw()

# message box display
messagebox.showinfo("Information", os.environ )

print(os.environ['HOME'])
