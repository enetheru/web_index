import os
import json
from pprint import pformat
from pprint import pprint

# Load Configuration
# ==================
'''
Usage:

    config = Config( 'hyperion' ).load()

default search paths
    /usr/share/hyperion/hyperion.conf
    /etc/hyperion.conf
    /etc/hyperion.d/*.conf
    $HOME/.hyperion.conf
    $HOME/.config/hyperion.conf
    $CWD/hyperion.conf


Configurations are json files which are all merged together, with latter ones
overriding earlier ones.

freedesktop standards: I looked in my own environemnt and found that there were
not anything referencing the XDG specification except for session ID's, so i
will stick to my custom heirarchy below for now.
'''

class Config:
    conf_list = []
    config = {}
    """Configuration Loader, uses standard search paths """
    def __init__( self, name ):
        self.name = name
        self.search_paths = []
        self.search_paths.append( "/usr/share/{0}/{0}.conf".format( name ) )
        self.search_paths.append( "/etc/{}.conf".format( name ) )
        self.search_paths.append( "/etc/{}.d/".format( name ) )
        self.search_paths.append(
                os.path.expandvars( "$HOME/.{}.conf".format( name ) ) )
        self.search_paths.append(
                os.path.expandvars( "$HOME/.config/{}.conf".format( name ) ) )
        self.search_paths.append(
                os.path.expandvars( "$HOME/.config/{}.d/".format( name ) ) )
        self.search_paths.append(
                os.path.expandvars( "$HOME/.config/personal/{}.conf".format( name ) ) )
        self.search_paths.append(
                os.path.expandvars( "{}/{}.conf".format( os.getcwd(), name ) ) )


    def load(self):
        print( "Search Paths:" )
        pprint( self.search_paths )
        for path in self.search_paths:
            if os.path.isfile( path ):
                self.conf_list.append( path );
                continue
            if os.path.isdir( path ):
                self.conf_list += [os.path.join( path, f )
                        for f in sorted( os.listdir( path ) )
                        if os.path.isfile( os.path.join( path, f ) )]
                continue

        self.config = {}
        for filename in self.conf_list:
            try:
                self.config.update( json.load( open(filename) ) )
            except json.JSONDecodeError as e:
                print("Failed to decode json" )
                print( e.msg )
                print( "at ({},{}) in {}".format( e.lineno, e.colno, filename ) )
                continue
        return self.config

    def add_path( self, path ):
        try:
            if os.path.exists( path ):
                self.search_paths.append( path )
        except:
            print( "{} is not a valid path".format( path ) )

    def __str__(self):
        string = 'config = {'
        string += '\nname={}'.format( self.name )
        string += '\nsearch_paths=' + pformat( self.search_paths )
        string += '\nconf_list=' + pformat( self.conf_list )
        string += '\nconfig =' + pformat(self.config)
        string += '\n}'
        
        return string
