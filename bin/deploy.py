#!/bin/env python


import json
import glob
import datetime
import sys, os
import argparse
import subprocess
import shlex
from pprint import pprint

# my modules
from config import Config

if __name__ == '__main__':
    # Argument Parsing
    #=================
    parser = argparse.ArgumentParser(description="Synchronise to cloud")

    parser.add_argument('-d','--dryrun', action='store_true',
            help="dont create json file")

    parser.add_argument('-c','--config', default='web-index',
            help="which config file to load")

    args = parser.parse_args()
    print("[INFO] Starting deploy.py")
    print( "[INFO] Program Options =" )
    pprint( json.dumps( vars(args), indent=2 ) )

    # Load Configuration
    # ==================
    config = Config( args.config ).load()
    #override with site config 
    with open(config['Content-Root'] + '/config.json') as f:
        config = {**config, **json.load(f) }
    pprint( config )

    print("Deploying to s3 bucket")
else:
    # define default variables here
    print("FIMXE add default variables if imported and they are not defined")


def deploy_aws( item ):
    #aws s3 sync ./ s3://enetheru-web-index --exclude "bin*" --exclude ".git*" --acl public-read
    cmd = 'aws s3 sync'
    if 'dryrun' in args:
        if args.dryrun is True:
            cmd += ' --dryrun'
    cmd += ' {}'.format( config['Content-Root'] )
    cmd += ' {}'.format( item['bucket'] ) #FIXME make this configurable
    for exclude in 'bin*','.git*': #FIXME Make these configurable
        cmd += ' --exclude "{}"'.format( exclude )
    cmd += ' --acl public-read'
    print( cmd )

    # FIXME add option for log file output.
    with subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT, bufsize=1) as p:
        #with open('logfile', 'wb') as logfile:
            for line in p.stdout:
                #logfile.write(line)
                sys.stdout.buffer.write(line)
                sys.stdout.buffer.flush()

def deploy_copy():
    print("TODO deploy_copy()")
    # the idea behind this is that it can be staged somewher else then copied
    # to a new location
    # I imagine that multiple copies might be possible.
    # which makes me think that there coule be multiple deploy actions on any
    # host and that the configuratin should reflect that
# TODO deploy to git
# TODO deploy to dropbox
# TODO deploy to google drive
# TODO deploy to other services.

def deploy():
    if 'deployments' in config:
        for item in config[ 'deployments' ]:
            pprint( item )
            if item['method'] == 'aws':
                deploy_aws( item )

if __name__ == '__main__':
    deploy()
