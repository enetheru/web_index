#!/usr/bin/env python
"""
Requires:
    python-magic 0.4.15 from ahupp's github repo to determine file types.
    https://github.com/ahupp/python-magic

    OpenImageIO Python3 Bindings

FIXME add this to the help documentation

Purpose:
    This module is to generate metadata about a file that can be exported as
    json or used directly.

Process
- analyse the file to generate new metadata
    - use basic filesystem
    - use magic/mime
    - use file extension
    - based on file type analyse and create entries

List of file categories, these are different than the mime type, such
that a photograph can be a jpeg but not all jpegs are photographs.
- photograph
- image
- vector
- 3d
- short video, ie snap or gif
- long video
- tweet
- text document
- article
- external link
- directory

What all files should have:
- name
- creation_date
- publication_date
- description
- category
- mimetype
- size
- thumbnail

Generation
----------
All Files
- use the modification timestamp from the file
- get the file mime type
- filesize
- TODO generate thumbnail
- TODO generate card background
- analyse any metadata for hashtags
Directory
- TODO folder content size
- TODO number of files
- TODO latest subfile change in the date_time field
Photograph
- resolution
- TODO megapixel count
- TODO gps location
- TODO content(macro, landscape, object, person, who, etc )
- TODO average colour?
- TODO edited/raw
- photo timestamp
- camera settings
Image
- resolution
- TODO content( meme, design, drawing, render )
vector
- ?
Gif/Snap
- TODO length
- TODO resolution
- TODO contnt { meme, lolcat, etc }
Video
- resolution
- bitrate
- running time
- Codecs
text/Tweet sized message
- ?
Raw Text
- ?
Article
- ?
External Link
- TODO always check for still active when generating database
HTML
- contains scripts?
- contains cookies
- number of lines?
- contains images and number of images?
- direct URL?
- I'm not really sure with this one
- broken links?
"""

from datetime import datetime
import sys, os
import pathlib
import json
import subprocess
import argparse

# for image/*
import OpenImageIO as oiio

# for text/html
from html.parser import HTMLParser

from urllib.parse import urlparse

import magic
from hurry.filesize import size

# Declare all my functions
# =============================================================================

# File Analysis
#==============

def analyse_image( entry ):
    print( "= Analysing image" )
    image = oiio.ImageInput.open( entry['path'] );
    if not image :
        print( '[ERROR] Could not open "{}"'.format( entry['path'] ) )
        print( "[ERROR]", oiio.geterror() )
        return

    spec = image.spec()
    image.close()

    print( "== Extra Metadata" )
    for attrib in spec.extra_attribs:
        print( "\t", attrib.name,"=", attrib.value )

    entry["channels"] = spec.nchannels
    entry["width"] = spec.width
    entry["height"] =  spec.height
    entry["pixelformat"] = str(spec.format)
    entry["channels"] = ""
    for chan in spec.channelnames:
        entry["channels"] += chan

    for attrib in spec.extra_attribs:
        # Camera and settings
        if attrib.name == 'Make':
            entry['Camera:Make'] = attrib.value
        if attrib.name == 'Model':
            entry['Camera:Model'] = attrib.value
        if attrib.name == 'DateTime':
            #FIXME parse date into date object first to verify
            entry["date_time"] = attrib.value

        # GPS and Time
        #if attrib.name == 'DateTime':
            #entry['DateTime'] = attrib.value
            #FIXME perform conversion
        #TODO Learn about GPS and how to nicely represent the data

    # TODO
    # Create Thumbnails
    return True


def analyse_markdown( entry ):
    print("[INFO] Analysing Markdown")

    markdown = open( entry['path'], 'r' )

    # word count
    entry['word_count'] = len(markdown.read().split())

    # line count
    markdown.seek(0)
    entry['line_count'] = len(markdown.read().split('\n'))

    # TODO No. sections and other statistics
    markdown.close();
    print("[INFO] Finished Analysis")
    return True

# ffmpeg generator
def analyse_video( entry ):
    cmd = "ffprobe -loglevel -8 -pretty -show_streams -show_format -of json "
    cmd += entry['path']
    print("[INFO] Analysing video with ffprobe")
    print("[INFO] CMD=", cmd, sep="" )

    process = subprocess.Popen( cmd, shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)

    # wait for the process to terminate
    out, err = process.communicate()
    errcode = process.returncode

    if errcode:
        print("[Error]subprocess for ffprobe returned:" , err )
        return False

    ffprobe = json.loads( out )
    print("[INFO] ffprobe Information" + json.dumps( ffprobe, indent=4 ) )
    format = ffprobe["format"]
    entry["format"] = format["format_long_name"]
    entry["duration"] = format["duration"].split('.')[0]
    entry["streams"] = format["nb_streams"]
    for stream in ffprobe["streams"]:
        if stream["disposition"]["default"] != 1:
            continue
        if stream["codec_type"] == "video":
            video_stream = stream
            continue
        if stream["codec_type"] == "audio":
            audio_stream = stream
            continue


    entry["video_codec"] = video_stream["codec_name"]
    entry["video_bitrate"] = video_stream["bit_rate"]
    entry["audio_codec"] = audio_stream["codec_name"]
    entry["audio_bitrate"] = audio_stream["bit_rate"]
    entry["lang"] = audio_stream["tags"]["language"]
    entry["samplerate"] = audio_stream["sample_rate"]
    entry["channels"] = audio_stream["channel_layout"]
    entry["width"] = video_stream["width"]
    entry["height"] = video_stream["height"]
    entry["aspect"] = video_stream["display_aspect_ratio"]
    entry["frame_rate"] = video_stream["avg_frame_rate"]

    #TODO generate a thumbnail
    return True

def analyse_dir( entry ):
    print("[TODO] analyse_dir")
    entry["mime"] = "inode/directory"
    #TODO pull the last modified date from its contents
    return False

#
# Analyse HTML
# ============
class MyHTMLParser(HTMLParser):
    title = False
    published = False
    def handle_starttag(self, tag, attrs):
        if tag == "title":
            self.title = True
        if tag == "published":
            self.published = True

    def handle_endtag(self, tag):
        if tag == "title":
            self.title = False
        if tag == "published":
            self.published = False

    def handle_data(self, data):
        if self.title:
            metadata["name"] = data
        if self.published:
            metadata["date_time"] = data



    def set_metadata( self, meta ):
        self.metadata = meta;


def analyse_html( entry ):
    print("[TODO] analyse_html")
    #get name from title of document

    document = open( entry['path'], 'r' )


    parser = MyHTMLParser()
    parser.set_metadata( meta=entry )
    parser.feed( document.read() )

    return False

# list of filetype generation functions
analysers = {}
analysers["application"] = {}
analysers["video"] = { 'default': analyse_video }
analysers["image"] = { 'default': analyse_image }
analysers['text'] = {
        'default': analyse_markdown,
        'html': analyse_html }


def is_url( url ):
    try:
        result = urlparse( url )
        return all([result.scheme, result.netloc])
    except ValueError:
        return False;

def analyse( path ):
    metadata = {'filename': os.path.basename( path ) }
    metadata['name'] = metadata['filename']
    metadata['path'] = os.path.abspath( path )

    if is_url( path ):
        #TODO - analyse URL
        exit()

    # else we assume that its a file path
    if not os.path.exists( path ):
        print( "[ERR] {} does not exist".format( path ) )
        return metadata

    # Timestamp
    dt = datetime.fromtimestamp( os.path.getmtime( path ) )
    metadata["date_time"] = dt.isoformat()

    # Test for folder and skip mime based analysis
    if os.path.isdir( path ):
        analyse_dir( metadata )
        return metadata

    # FileSize
    metadata["size"] = size( os.path.getsize( path ) )

    # mime type
    metadata["mime"] = magic.from_file( path, mime = True )

    # Analyse the file contents depending on its mime type:
    primary, secondary = metadata['mime'].split( '/' )
    if primary in analysers:
        if 'default' in analysers[primary]:
            analysers[primary]['default']( metadata )
        if secondary in analysers[primary]:
            analysers[primary][secondary]( metadata )

    # strip path from metadata
    del metadata['path']
    return metadata



# Script Actions
# ==============
if __name__ == "__main__":
    # execute only if run as a script
    # Argument Parsing
    #=================
    parser = argparse.ArgumentParser(description="analyse file and generate metadata")

    parser.add_argument('path',nargs='?',
            help="folder to operate on")

    args = parser.parse_args()

    metadata = analyse( args.path )

    with open( args.path + ".json", 'w') as metafile:
        metafile.write( json.dumps( metadata, indent=2 ) )
