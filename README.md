web-index
=========
its a lame name.. but its just an experiment with programming a thing.

Purpose
-------
Generically: Publish and display content in a website.
Personally: A way to express myself away from major platforms influence.
I am not happy with the web as it currently stands, I saw a thread on reddit a
while ago about what you would change about the internet, and the top 10
answers were to return to the web of the 90's with personal websites etc.. back
when the web was an explorers heaven, going down rabbit holes of recommended
links etc.. i also remember the times before google and the ubiquitousness of
DMCA takedowns.. if i can separate myself from the major platforms then i dont
have to worry about such things and can focus on creating my own content.

Display Concept
---------------
A re-imagining of the generated directory listing html

if we take the concepts from file managers like windows explorer, nautilus,
pcmanfm, caja, thunar, dolphin?, etc.. and make them the read only publishing
platform for my web.. made to suit my specific aethetic.

High level Workflow
===================

## Create New Content
A two letter combination 'we' for 'web entry' which opens a text document,
could also open any other type of file, but for now we'll stick to markdown
text. and optionally with a template

after writing, editing, and reviewing the content, the option to publish it to
the site. which generates the metadata and copies it to the correct folders

*The Primary Purpose is to get up and running as fast and easily as possible*

## Publish Existing Content
### from a GUI tool
1. right click
2. select publish to web index
3. optionally edit the metadata

### From the cmd line
```bash
# we-publish filename
```

## under the hood process
Whether the file is created right then or pre-existing
* Firstly the file is copied to/created in the scratch area
* a matching metadata file is created through file analysis
* the option to edit the metadata is available
* the file is copied from the scratch area into the content area
* the collated metadata database is rebuilt
* its all pushed to clones.

Metadata
========
In order to display all the information and gather statistics easily, each
published object will be analysed and the metadata stored in json.

File metadata
-------------
each file will have its own metadata file will be like this
subject  : ./myfile.jpeg
metadata : ./myfile.jpeg.json

it includes anything that i think would be useful to have, each filetype will
have different things that will be interesting. so far its just a flat
structure like this
```json
{
    "name":"myname",
    "filename":"filename.ext",
    "filesize":"1024kb",
    "created":datetime,
    "modified":datetime,
    etc...
}
```

metadata database
-----------------
to make it easy for the page to load, there will also be a collated db.json in
each folder, which will contain a concatenation of the individual json files
into an array. as well as some additional metadata about the database and
folder itself.

```json
{
    ... some db info here
    "list": [... concatenated json files. ]
}
```

website operates with the context of a local folder, so that all url's are
relative to the folder

context = some-folder
context/metadb.js
context/files
context/files.json

this ill make it so that when you click on a folder, it changes the context,
flushes the database and reloads the new one from the context folder. you
basically never leave index.html

Controls
========
i wish to incorporate sorting and filtering, breadcrumb history and path, and
anything else that might make navigating around easier. even searching.
I'll expand on each of these when i have the inspiration.

Filtering
---------
* regex
* attribute: tag=pattern eg type=img, category=
* chaining would be nice

Sorting
-------
* forward and reverse
* by attribute
* Chaining
* number and date aware

pwd
---
current directory tree

breadcrumb
----------
history of locations.



Viewport
========
the viewport is the place that the file listing is displayed, and there are
quite a number of ways that this can be done.

* tabular.js
* grid.js
* cover.js
* radial.js
* square.js
* timeline.js
* tree.js

Tabular
-------
A detailed listing with column headers and displays like a table

Grid Layout
-----------
* icon
* thumbnail
* card
* list
part of the idea with the grid layout is that i want the cotents of the items
to be customisable, using css or something, that way people can style it
however they want.

Cover Browser
-------------
just like a media centre

Radial graph
------------
inspired by baobab

Square graph
------------
inspired by spacesniffer

Timeline
--------
could be animated, etc. is a nice addition to the

Tree Graph
----------
typical tree based view

git-view
--------
might aswell make custom views that are for viewing types of folder trees, with
timelines and branches etc..

Python Scripts/Modules
======================
## analyse.py
importable module that auto generates the metadata for the objects
can be imported to use as a function or run as a command in the shell.

## collate.py
Used to join all the metadata items in a directory into a single database.
can be imported to use as a function or run as a command in the shell.

## create.py
helper script that opens a text editor with an optional template, automatically
analyses the result after saving and publishes to the site.

## deploy.sh
pushes changes to the connected duplicates



Filemanager Scripts
===================
## Caja Publish
performs all necessary steps to publish an object to the site

future concepts
===============
I imagine a time in the future that i might be able to have the selection from
one view lead into another view, and then display the file all within the same
window. that way i can have a tree view select folders, then a icon or
thumbnail view, then a viewer just like a modern file manager. all client side.

If i can make it nice enough, i can forsee it overshadowing the concepts in
traditional file managers. I will work towards my vision in the hopes that it
may influence existing products eventually.

Node Editor
-----------
i can sort of imagine a way to use a sort of node editor to buildup a set of
filter rules and display the output as a listing. its a sort of generic concept
but fits in my mind quite well.. i've wanted to play with node editors for a
long time. and eventually i will get there.

tiling area manager
-------------------
if i'm going to split the view to use multiple viewpanes that filter the
outputs into each other then i need a way to display the viewports. a tiling
based area manager would be a nice one, and have connection lines, so that the
output of one view can be piped into another one.

staged publishing
-----------------
in my context, i wish for there to be three stages to publishing,
* to my local copy on my laptop
* to my rpi webserver in my home
* to the online s3 bucket or alternate online server.

at each stage there may or may not be an internet connection so there will have
to be some detection of whether the host is available and then a
synchronisation step.

also depending on the host, i dont want all things to go to the same place. for
instance, video's should upload to a video sharing site, either or both
youtube, or vimeo, or whatever. then a link to that site rather than the file
itself. to save hosting bandwidth obviously.



